Rails.application.routes.draw do
  root "churches#index"
  
  resources :users
  resources :churches
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
