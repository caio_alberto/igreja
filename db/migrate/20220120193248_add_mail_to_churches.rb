class AddMailToChurches < ActiveRecord::Migration[6.1]
  def change
    add_column :churches, :mail, :string
  end
end
