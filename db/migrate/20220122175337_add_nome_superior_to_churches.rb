class AddNomeSuperiorToChurches < ActiveRecord::Migration[6.1]
  def change
    add_column :churches, :nome_superior, :string
  end
end
